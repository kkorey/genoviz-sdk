/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.affymetrix.genoviz.glyph;

import com.affymetrix.genoviz.bioviews.ViewI;
import static com.affymetrix.genoviz.glyph.EfficientSolidGlyph.drawDirectedLine;
import com.affymetrix.genoviz.util.NeoConstants;
import java.awt.Graphics;
import java.awt.Rectangle;
import static com.affymetrix.genoviz.bioviews.Glyph.optimizeBigRectangleRendering;

/**
 *
 * @author dcnorris
 */
public class EfficientMateJoinGlyph extends EfficientLineContGlyph {

    @Override
    public void draw(ViewI view) {
        Rectangle pixelbox = view.getScratchPixBox();
        view.transformToPixels(this.getCoordBox(), pixelbox);
        if (pixelbox.width == 0) {
            pixelbox.width = 1;
        }
        if (pixelbox.height == 0) {
            pixelbox.height = 1;
        }

        Graphics g = view.getGraphics();
        g.setColor(getBackgroundColor());

        int y = pixelbox.y + ((3 * pixelbox.height) / 4);
        drawDirectedLine(g, pixelbox.x, y, pixelbox.width, NeoConstants.NONE);

    }
}
